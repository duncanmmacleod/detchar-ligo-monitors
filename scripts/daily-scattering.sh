#!/bin/bash
#
# Run the gwdetchar.scattering module on a daily stride

export MPLCONFIGDIR=${HOME}/.matplotlib
. ~/.bash_profile
if [[ "$(whoami)" == "detchar" ]]; then
    unset X509_USER_PROXY
fi
set -e


# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# set python environment
activate_conda_environment
echo "-- Environment set"
echo "Conda prefix: ${CONDA_PREFIX}"

# set condor accounting information
# these should match the parameters in the /condor/daily-scattering.sub file
# and are picked up by gwdetchar.omega.batch for the omega scans

if [[ "$(whoami)" == "detchar" ]]; then
    export _CONDOR_ACCOUNTING_TAG="ligo.prod.o4.detchar.daily.summary"
    export _CONDOR_ACCOUNTING_USER="evan.goetz"
else
    export _CONDOR_ACCOUNTING_TAG="ligo.prod.o4.detchar.daily.summary"
    export _CONDOR_ACCOUNTING_USER="$(whoami)"
fi

# get run date
gpsstart=`gps_start_yesterday`
gpsend=`gps_start_today`
date_=`yyyymmdd ${gpsstart}`
duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/scattering/
outdir=${htmlbase}/day/${date_}
mkdir -p ${outdir}
cd ${outdir}
echo "-- Moved to output directory: `pwd`"

# get IFO
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi

# get analysis flag
FLAG="${IFO}:DMT-GRD_ISC_LOCK_NOMINAL:1"

# set parameters
NPROC=4
FREQUENCY="15"

# run the code
set +e

nagios_status 0 "Daily scattering analysis for ${date_} is running" 10000 "Daily scattering analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

LOGFILE="/tmp/daily-scattering-$(whoami).err"
cmd="python -m gwdetchar.scattering $gpsstart $gpsend --ifo ${IFO} --main-channel ${IFO}:GDS-CALIB_STRAIN --frametype ${IFO}_R --state-flag ${FLAG} --nproc ${NPROC} --frequency-threshold ${FREQUENCY} --sigma 6 --omega-scans 5 --verbose"
echo "$ $cmd" && eval $cmd 2> ${LOGFILE}

EXITCODE="$?"

# write JSON output
TIMER=100000
TIMEOUT="Daily scattering analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily scattering analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily scattering analysis for ${date_} failed with exitcode $EXITCODE\\\n" && sed ':a;N;$!ba;s/\n/\\\n/g' ${LOGFILE} | tr '"' "'"`
    nexit=2
fi

nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
